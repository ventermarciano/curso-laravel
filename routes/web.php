<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('pais/cadastro', 'PaisController@exibirCadastro')->name('pais-cadastro');
//Route::get('cidade/lista', 'CidadeController@exibirCidadesEstadosPaises')->name('cidade-lista');
Route::post('pais/cadastrar', 'PaisController@cadastrar')->name('pais-cadastrar');
Route::get('pais/lista', 'PaisController@exibirPaises')->name('pais-lista');