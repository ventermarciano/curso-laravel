<?php

use Illuminate\Database\Seeder;

class PaisSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paises = array(
            ['nome' => 'Brasil', 'sigla' => 'BRA', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['nome' => 'Alemanha', 'sigla' => 'ALE', 'created_at' => new DateTime, 'update_at' => new DateTime]
            );
            DB::table('pais')->insert($paises);
            $this->command->info('Cadastrando Paises!');
    }
}
