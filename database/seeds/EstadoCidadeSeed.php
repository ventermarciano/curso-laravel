<?php

use Illuminate\Database\Seeder;

class EstadoCidadeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estado = array(
            ['idPais' => 1, 'nome' => 'Rio Grande do Sul', 'sigla' => 'RS', 'regiao' => 'Sul','created_at' => new DateTime, 'updated_at' => new DateTime],
            ['idPais' => 1, 'nome' => 'Paraná', 'sigla' => 'PR', 'regiao' => 'Sul', 'created_at' => new DateTime, 'updated_at' => new DateTime]
            );
            DB::table('estado')->insert($estado);
            $this->command->info('Cadastrando Estados!');
            
        $cidade = array(
            ['idestado' => 1, 'nome' => 'Três Coroas', 'cep' => '95660-000', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['idestado' => 2, 'nome' => 'Curitiba', 'cep' => '', 'created_at' => new DateTime, 'updated_at' => new DateTime]
            );
            DB::table('cidade')->insert($cidade);
            $this->command->info('Cadastrando Cidades!');
    }
}
