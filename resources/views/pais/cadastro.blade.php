@extends('layout')

@section('conteudo')

<h1> Cadastro de pais</h1>

<form class="form-inline" action="{{route('pais-cadastrar')}}" method="POST">
    {{ csrf_field() }}
    
    <div class="form-group">
        <label class="sr-only" for="nome"> Nome:</label>
        <input class="form-control" type="text" name="nome" value="" id="nome" placeholder = "Nome" />
        
    </div>
    <div class="form-group">
        <label class="sr-only" for="sigla">Sigla:</label>
        <input class="form-control" type="text" name="sigla" value="" id="sigla" placeholder = "Sigla"/>
    </div>
    <div class="form-group">
        <button class="btn btn-primary" type="submit" aria-label="Cadastrar">
        <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>
        Cadastrar
        </button>
        <button class="btn btn-danger" type="button" aria-label="Voltar" />
        <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
        Voltar
        </button>
    </div>
    
</form>



@endsection

