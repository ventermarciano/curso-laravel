<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Pais;

class PaisController extends Controller
{
    public function exibirCidades() {
        $listaDeCidades = Cidade::all();
        foreach ($listaDeCidades as $cidadepais) {
          foreach ($pais->estados as $estado) {
              var_dump($estado->pais->nome);
          }
          
        };
        
        //return view('pais.lista', ['paises' => $listaDePaises]);
    }
    
    public function exibirPaises() {
        $paises = Pais::all();
        return view('pais.lista', ['paises' => $paises]);
    }
    
    public function cadastrar() {
        $pais = new Pais();
        $pais->nome = Input::get('nome');
        $pais->sigla = Input::get('sigla');
        $pais->save();
        
        return redirect(route('pais-lista'));
    }
    
    public function exibirCadastro() {
        return view('pais.cadastro');
    }
}

